import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';

@Component({
  selector: 'app-tab4',
  templateUrl: 'tab4.page.html',
  styleUrls: ['tab4.page.scss']
})
export class Tab4Page {

   //Déclaration variable
   searchTitle:'';
   movieApiUrl = '';
   movieData = {
     title:'',
     description:'',
     imageURL:''
   };
   //Constructeur
   constructor(public http: HttpClient) {
    
   }
 
   //Function Lire API 
   readAPI(URL:string){
     return this.http.get(URL);
   }
 //Fonction pour la barre recherche
 
   searchMovie(){
   //creation constance temporaire pour que l'appel API reçois bien l'info par rapport aux espaces et caractere
   const search = encodeURIComponent(this.searchTitle).trim();
   console.log('recherche du film ' + search);
   this.movieApiUrl = 'http://www.omdbapi.com/?i=tt3896198&apikey=7c96afb&t=' + search ;
   this.readAPI(this.movieApiUrl).subscribe((data) =>{
    console.log(data);
    this.movieData.title = data['Title'];
    this.movieData.description = data['Plot'];
    this.movieData.imageURL = data['Poster'];
  });
 }
 

}

import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { InscriptionPage } from '../inscription/inscription.page'
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
   //Déclaration variable
   movies = [];
   moviesList:any; 
   
   constructor(public http: HttpClient, public modalController: ModalController) {
     this.db('http://localhost/wefilm/src/php/getMovies.php').subscribe((data) => {
       console.log(data);
       this.movies= data[0];
       console.log(this.movies);
     });
     this.allMovies('http://localhost/wefilm/src/php/getMovieRec.php').subscribe( data => {
      this.moviesList = data; 
      }, err =>{
      console.log(err); 
      }); 
   }
   db(URL: string) {
     return this.http.get(URL);
   }
   allMovies(URL: string) {
    return this.http.get(URL);
  }

}
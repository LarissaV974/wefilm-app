import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { InscriptionPage } from '../inscription/inscription.page'
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-partieadmin',
  templateUrl: './partieadmin.page.html',
  styleUrls: ['./partieadmin.page.scss'],
})
export class PartieadminPage{

  films ={
    title:'',
    images:'assets/'+'',
  };
  film :any;
  FilmsData:any;

  constructor(public http: HttpClient, public modalController: ModalController, public navCtrl : NavController, private router: Router) { 
    this.DataMovies('http://localhost/wefilm/src/php/getMovies').subscribe( data => {
      this.FilmsData = data;
      console.log(data);  });
  }
  addFilm(){
  this.dbFilms('http://localhost/wefilm/src/php/getMovies').subscribe( data => {
    this.FilmsData = data;
    console.log(data);  });
  this.Insertdata('http://localhost/wefilm/src/php/insertMovie.php',this.films).subscribe( data => {
    this.film = data;
      console.log(this.films.title);
      console.log(this.films.images);
      alert("Le film a bien été ajouté");
      this.router.navigate(['../partieadmin/']);
    });
}
Insertdata(URL: string,client) {
  return this.http.post(URL,this.films,this.film);
}
dbFilms(URL: string) {
  return this.http.get(URL);
}
DataMovies(URL: string){
  return this.http.get(URL); 
}

}
